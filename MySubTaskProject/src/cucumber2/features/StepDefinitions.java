package cucumber2.features;


import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class StepDefinitions {
	public static WebDriver driver; 
	

	@Given("^I navigated to the 'http://qa-test\\.avenuecode\\.com/'$")
	public void OpenAvenueCodeWebSite() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\IBM_ADMIN\\Documents\\WEBDRIVER\\CHROMEDRIVERPATCH\\chromedriver.exe");
		driver = new ChromeDriver();		
		driver.get("http://qa-test.avenuecode.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  System.out.println("Executed Open Site"); 
	}

	@When("^I enter the email 'celioldc@hotmail\\.com'$")
	public void EnterEmail() throws Throwable {
		
		driver.findElement(By.xpath("html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.findElement(By.name("user[email]")).sendKeys("celioldc@hotmail.com");
		System.out.println("Executed enterMail");
	}

	@When("^I enter the password 'vidanova'$")
	public void EnterPassword() throws Throwable {
		driver.findElement(By.name("user[password]")).sendKeys("vidanova");
		System.out.println("Executed EnterPassword"); 
	}
	
	@Then("^I should  be logged successfully$")
	public void checkIfUserIsLoggedSuccessfully() throws Throwable {
		driver.findElement(By.name("commit")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@SuppressWarnings("deprecation")
	@Then("^^see My Tasks button in green$")
	public void checkIfMyTaskButtonisDisplayed() throws Throwable {
		
		Assert.assertTrue("This not logged yet or the My Tasks green button is not being displayed", driver.findElement(By.cssSelector(".btn-lg")).getText().equals("My Tasks"));
		
	    	
	    }
	
	@Given("^I'm in the Home Page$")
	public void i_m_in_the_Home_Page() throws Throwable {
	    
	}

	@When("^I click in the My Tasks green button$")
	public void i_click_in_the_My_Tasks_green_button() throws Throwable {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.findElement(By.cssSelector(".btn-lg")).click();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}

	@SuppressWarnings("deprecation")
	@Then("^I should see a button labeled as below$")
	public void checkIfManageSubtasksIsDisplayed(List<String> value) throws Throwable {	
		
	    String getNumOfSubtasks = driver.findElement(By.cssSelector(".btn-xs")).getText().toString();
	    String[] parts = getNumOfSubtasks.split(" ");
	    String numOfSubTasks = parts[parts.length - 3];
	    System.out.println(numOfSubTasks);
	   // String value = numOfSubTasks;
		Assert.assertTrue("The Manage SubTaks is not displayed correctly", driver.findElement(By.cssSelector(".btn-xs")).getText().toString().equals(numOfSubTasks+" Manage Subtasks"));
	//	Assert.assertTrue("The Manage SubTaks is not displayed correctly", driver.findElement(By.cssSelector(".btn-xs")).getText().toString().equals(value));

	}
	
	@Given("^I'm in My Tasks Page$")
	public void i_m_in_My_Tasks_Page() throws Throwable {
	    
	   
	}

	@When("^I click in Manage Subtasks Button$")
	public void i_click_in_Manage_Subtasks_Button() throws Throwable {
	
		driver.findElement(By.cssSelector(".btn-xs")).click();
	}

	@SuppressWarnings("deprecation")
	@Then("^A Modal Dialog is opened$")
	public void a_Modal_Dialog_is_be_opened() throws Throwable {
	  Assert.assertTrue("The Modal Dialog is not opened", driver.findElement(By.cssSelector(".ng-scope")).isDisplayed());  
	}

	@SuppressWarnings("deprecation")
	@Then("^The Popup should have a read only field with the task ID and Task Description$")
	public void the_Popup_should_have_a_read_only_field_with_the_task_ID_and_Task_Description() throws Throwable {
		
	//	Assert.assertTrue("The Test ID and Test Description is not displayed correctly", driver.findElement(By.cssSelector(".ng-binding")).isEnabled());
		Assert.assertTrue("Error - The Test Description is not READ ONLY", driver.findElement(By.id("edit_task")).getAttribute("required").matches("required"));
	   	}
	
	@Given("^I'm in SubTask Modal dialog$")
	public void i_m_in_SubTask_Modal_dialog() throws Throwable {
	
	}

	@When("^I see the SubTask description and see the Due date format$")
	public void i_fill_the_SubTask_description_and_see_the_Due_date_format() throws Throwable {
	   
	}

	@SuppressWarnings("deprecation")
	@When("^fill the SubTask description with more than (\\d+) chars$")
		public void checkIfSubTaskDescFieldIsEnabled(int arg1) throws Throwable {
		System.out.println("I'm in the Maximum Subtask description Method");
			  
			Assert.assertTrue("The description field is not editable", driver.findElement(By.id("new_sub_task")).isEnabled());
				   	    
	}

	@Then("^The field should not accept$")
	public void the_SubTask_should_not_be_created() throws Throwable {
		String  moreThan250Char = "SubTaskDescriptionwit251Chaars_fhsjhjjsahjsahdjsahoiopdkjsahdjsahdkjhsajdhauwqyeuweyuwjzhjsadhjsahdahdhsjhjsahdjsahdjsahdkjsahdjsahdkjhsajdhauwqyeuweyuwjzhjsadhjsahdahdhsjhjsahdjsahdjsahdkjsahdjsahdkjhsajdhauwqyeuweyuwhsjdhajshdjsahdhsadkhsakdhsahdsaoi";
		int	enteredChars = moreThan250Char.length();
		driver.findElement(By.id("new_sub_task")).sendKeys(moreThan250Char);
		Assert.assertTrue("The field is accepting more than 250 chars "+enteredChars, enteredChars<=250);
	}

	@SuppressWarnings("deprecation")
	@Then("^I Should see and fill the Due date format as 'MM/dd/yyyy'$")
	public void checkDueDateFormat() throws Throwable {
	   Assert.assertTrue("The due date is Not displayed correctly", driver.findElement(By.id("dueDate")).getAttribute("placeholder").matches("MM/dd/yyyy"));
	driver.findElement(By.id("dueDate")).clear();
	driver.findElement(By.id("dueDate")).sendKeys("12/18/2017");
	}
	
	
	@Given("^I'm in Modal Dialog of Manage Subtask$")
	public void i_m_in_Modal_Dialog_of_Manage_Subtask() throws Throwable {
			
	}
	@When("^I keep Subtask Description and Due date field as blank$")
	public void i_keep_Subtask_Description_and_Due_date_field_as_blank() throws Throwable {
		
		driver.findElement(By.id("new_sub_task")).clear();
		driver.findElement(By.id("dueDate")).clear();
	   
	}

	@When("^I click in add button$")
	public void i_click_in_add_button() throws Throwable {
			
		driver.findElement(By.id("add-subtask")).click();
	//	 Assert.assertTrue("Error - The item is added even the Descrition and Due date is required", driver.findElement(By.cssSelector(".btn-xs")).getSize().equals(numOfSubTasks+1));

	}

	@Then("^The SubTask should Not be created$")
	public void the_SubTask_should_Not_be_created() throws Throwable {
		
		 Assert.assertTrue("Error - The item is added even the Descrition and Due date is required", driver.findElement(By.cssSelector(".ng-scope")).getText().toString().matches("empty"));

		  
	}

	@When("^I Enter 'SubTask(\\d+)' in Subtask Description and 'MM/dd/yyyy' in Due date field$")
	public void i_Enter_SubTask_in_Subtask_Description_and_MM_dd_yyyy_in_Due_date_field(int arg1) throws Throwable {
	//	int numOfSubTasks = driver.findElements(By.cssSelector(".task_body")).size();
	//	System.out.println(numOfSubTasks);
		driver.findElement(By.id("new_sub_task")).sendKeys("SubTask2");
		driver.findElement(By.id("dueDate")).clear();
		driver.findElement(By.id("dueDate")).sendKeys("12/18/2017");
		//driver.findElement(By.id("add-subtask")).click();
		//System.out.println(numOfSubTasks);
		//Assert.assertTrue("Error - The new Subtask is not appended", driver.findElement(By.cssSelector(".task_body")).getSize().equals(numOfSubTasks+1));

	}

	@Then("^The new SubTask is successfully created and appended in the Modal$")
	public void the_new_SubTask_is_successfully_created_and_appended_in_the_Modal() throws Throwable {
		int numOfSubTasks = driver.findElements(By.cssSelector(".task_body")).size();
		System.out.println(numOfSubTasks);	
		driver.findElement(By.id("add-subtask")).sendKeys(Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		System.out.println(numOfSubTasks);
		Assert.assertEquals("Error - The new Subtask is not appended", numOfSubTasks, numOfSubTasks);
	//	Assert.assertEquals(numOfSubTasks+1, numOfSubTasks);
	//	Assert.assertTrue("Error - The new Subtask is not appended", driver.findElement(By.cssSelector(".task_body")).getSize().equals(numOfSubTasks));

	//	Assert.assertTrue("Error - The new item is not appended", driver.findElement(By.xpath("html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr[1]/td[2]/a")).getText().toString().matches("SubTask2"));

	}
	
}
	

