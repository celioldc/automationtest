Feature: Create Subtask
  As a ToDo App user
  I should be able to create a subtask
  So I can break down my tasks in smaller pieces

  Scenario: Login into site and see My task Link
    Given I navigated to the 'http://qa-test.avenuecode.com/'
    When I enter the email 'celioldc@hotmail.com'
    And I enter the password 'vidanova'
    Then I should  be logged successfully
    And see My Tasks button in green

  Scenario: Check Manage Subtasks label
    Given I'm in the Home Page
    When I click in the My Tasks green button
    Then I should see a button labeled as below
      | ('NumberOfSubTasks') Manage Subtasks |
     
      |                     |
     
  Scenario: Check Modal Dialog from Manage Subtasks
    Given I'm in My Tasks Page
    When I click in Manage Subtasks Button
    Then A Modal Dialog is opened
    And The Popup should have a read only field with the task ID and Task Description
    
    Scenario: Check SubTask Description and Due date field
    Given I'm in SubTask Modal dialog
    When I see the SubTask description and see the Due date format  
    And fill the SubTask description with more than 250 chars
    Then The field should not accept
    And I Should see and fill the Due date format as 'MM/dd/yyyy'
    
    
  Scenario: Try to create a new SubTask without fill Description and Due date
    Given I'm in Modal Dialog of Manage Subtask
    When I keep Subtask Description and Due date field as blank
    And I click in add button
    Then The SubTask should Not be created
    
  Scenario: Create a new SubTask filling Description and Due date field
    Given I'm in Modal Dialog of Manage Subtask
    When I Enter 'SubTask1' in Subtask Description and 'MM/dd/yyyy' in Due date field
    And I click in add button
    Then The new SubTask is successfully created and appended in the Modal
  