package cucumber1.features;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class StepDefinitions {
	public static WebDriver driver; 
	
	@Given("^I navigated to the 'http://qa-test\\.avenuecode\\.com/'$")
	public void OpenAvenueCodeWebSite() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\IBM_ADMIN\\Documents\\WEBDRIVER\\CHROMEDRIVERPATCH\\chromedriver.exe");
		driver = new ChromeDriver();		
		driver.get("http://qa-test.avenuecode.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  System.out.println("Executed Open Site"); 
	}

	@When("^I enter the email 'celioldc@hotmail\\.com'$")
	public void EnterEmail() throws Throwable {
		
		driver.findElement(By.xpath("html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		driver.findElement(By.name("user[email]")).sendKeys("celioldc@hotmail.com");
		System.out.println("Executed enterMail");
	}

	@When("^I enter the password 'vidanova'$")
	public void EnterPassword() throws Throwable {
		driver.findElement(By.name("user[password]")).sendKeys("vidanova");
		System.out.println("Executed EnterPassword"); 
	}
	
	@Then("^I should  be logged successfully$")
	public void checkIfUserIsLoggedSuccessfully() throws Throwable {
		driver.findElement(By.name("commit")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}


	
	@SuppressWarnings("deprecation")
	@Then("^see ?My Tasks? link on the NavBar$")
	public void checkIfMyTaskLinkisDisplayed() throws Throwable {
		
		Assert.assertTrue("This not logged", driver.findElement(By.xpath("html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a")).getText().equals("My Tasks"));
		
	    	
	    }
	@Given("^I clicked to 'My Tasks' link$")
	public void clickInMyTasksLink() throws Throwable {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.findElement(By.xpath("html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a")).click();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}

	@SuppressWarnings("deprecation")
	@Then("^I should  see the message Hey celio leandro, this is your todo list for today:$")
	public void checkIfCorrectMessageIsDisplayed() throws Throwable {
	
		driver.findElement(By.partialLinkText("My Tasks")).click();
		
		
Assert.assertTrue("The User list message is Not displayed correctly", driver.findElement(By.xpath("html/body/div[1]/h1")).getText().toString().equals("Hey celio leandro, this is your todo list for today:"));
	}
	
	@Given("^I go to Task description input field$")
	public void goToInputTaskField() throws Throwable {
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).click();  
	}

	@When("^I enter 2 chars$")
	public void keepEmpty() throws Throwable {
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).sendKeys("ab");
	}

	@SuppressWarnings("deprecation")
	@When("^hit enter$")
	public void hitEnter() throws Throwable {
		int numOfTasks = driver.findElements(By.className("ng-scope")).size();	
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).sendKeys(Keys.ENTER);
		
		 Assert.assertTrue("Error - The item is added even the minumum of 3 chars is required", driver.findElement(By.className("ng-scope")).getSize().equals(numOfTasks+1));
		
		
	}

	@Then("^I Must not be able to add a new task once the minimum (\\d+) chars is required$")
	public void checkMinumumCharAllowed(int arg1) throws Throwable {
		
 
	}

	@When("^I type (\\d+) chars$")
	public void enterManyChars(int arg1) throws Throwable {
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).sendKeys("abcdefghijklmnosjdlaksjdlkjsdjsjdjsjdajsdjsaljdsajldjsaldjlsajdlksajdsajdjsadjsajdsajdlkjsadsadsadsadsadsadsadsadsadsadsadsadsadsadsadsadsadsdsadsadsadsadsadsadsadsadsadsadsadsadsadsadsadsadsadsaddsadsadsadsadsadadsaddsadadsadadsaddsaddasdsadsadsadsad"); 
	}

	@SuppressWarnings("deprecation")
	@When("^I press Add button$")
	public void pressAddButton() throws Throwable {
		int numOfTasks = driver.findElements(By.className("ng-scope")).size();
	  driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/span")).click(); 
		 
	  Assert.assertTrue("Error - The item is added even the maximum of 250 chars is required", driver.findElement(By.className("ng-scope")).getSize().equals(numOfTasks+1));

	}

	@Then("^I Must not be able to add a new task once the maximum of chars is (\\d+)$")
	public void checkMaximumCharAllowed(int arg1) throws Throwable {
	   
	}
				
	@When("^I type Task (\\d+)$")
	
	public void typeTask1(int arg1) throws Throwable {
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).sendKeys("Task1");

		
	}

	
	@When("^I hit enter$")
	public void hitEnter1() throws Throwable {
	
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).sendKeys(Keys.ENTER);
		driver.navigate().refresh();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

	
	}

	@SuppressWarnings("deprecation")
	@Then("^I Must be able to add a new task$")
	public void checkIfNewTaskIsAddedAndAppended1() throws Throwable {
		Assert.assertTrue("Error - The new item is not appended", driver.findElement(By.xpath("html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/a")).getText().equals("Task1"));
		
	}

	@Given("^I go back to Task description input field$")
	public void goBackToDescriptionField() throws Throwable {
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).click();
		

	}

	@When("^I type new Task (\\d+)$")
	public void typeTask2(int arg1) throws Throwable {
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/input")).sendKeys("Task2");
	}

	@When("^I click in Add button$")
	public void pressAddButton2() throws Throwable {

		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[1]/form/div[2]/span")).click();
		driver.navigate().refresh();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

	}

	@SuppressWarnings("deprecation")
	@Then("^I Must be able to add a new one task$")
	public void checkIfNewTaskIsAddedAndAppended2() throws Throwable {
	   
		Assert.assertTrue("Error - The new item is not appended", driver.findElement(By.xpath("html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/a")).getText().equals("Task2"));

	}
	
	}


