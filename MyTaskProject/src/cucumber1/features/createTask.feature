Feature: Create Task
As a ToDo App user
I should be able to create a task
So I can manage my tasks

Scenario: Login into site and see My task Link
Given I navigated to the 'http://qa-test.avenuecode.com/'
When I enter the email 'celioldc@hotmail.com'
And I enter the password 'vidanova'
Then I should  be logged successfully
And see My Tasks link on the NavBar

Scenario: Message saying the User Logged to
Given I clicked to 'My Tasks' link
Then I should  see the message Hey celio leandro, this is your todo list for today:

Scenario: CheckAddButtonMinimumChars
Given I go to Task description input field
When I enter 2 chars 
And hit enter
Then I Must not be able to add a new task once the minimum 3 chars is required

Scenario: CheckAddButtonMaximumChars
Given I go to Task description input field
When I type 251 chars  
And I press Add button
Then I Must not be able to add a new task once the maximum of chars is 250

Scenario: addNewtask1
Given I go to Task description input field
When I type Task 1  
And I hit enter

Scenario: addNewtask2
Given I go back to Task description input field
When I type new Task 2  
And I click in Add button
Then I Must be able to add a new one task